﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TPS.TASK.DAL;
using TPS.TASK.Repository;

namespace TPS.TASK.BLL
{
    public class taskBLL
    {
        TaskDAL sTaskDAL = new TaskDAL();

        internal int SaveTask(tbl_Task stbl_Task)
        {
            return sTaskDAL.SaveTask(stbl_Task);
        }

        internal List<tbl_Task> GetAllTask()
        {
            return sTaskDAL.SaveGetAllTaskTask();
        }

        internal List<tbl_Task> GetTaskById(string taskId)
        {
            return sTaskDAL.GetTaskById(taskId);
        }

        internal int UpdateTask(tbl_Task stbl_Task, long _Id)
        {
            return sTaskDAL.UpdateTask(stbl_Task, _Id);
        }

        internal int SaveTask(tbl_TaskPosting stbl_TaskPosting)
        {
            return sTaskDAL.SaveTask(stbl_TaskPosting);
        }

        internal List<taskR> GetAllPstingData()
        {
            return sTaskDAL.GetAllPstingData();
        }


        internal List<tbl_TaskPosting> GetTotallikeCount(int id)
        {
            return sTaskDAL.GetTotallikeCount(id);
        }

        internal int LikeUpdate(int id, int likeIncrese)
        {
            return sTaskDAL.LikeUpdate(id, likeIncrese);
        }

        internal int DisLikeUpdate(int id, int DislikeIncrese)
        {
            return sTaskDAL.DisLikeUpdate(id, DislikeIncrese);
        }
    }
}
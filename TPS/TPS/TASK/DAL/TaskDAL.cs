﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TPS.TASK.Repository;




namespace TPS.TASK.DAL
{
    public class TaskDAL
    {
        TaskPostingEntities sTaskPostingEntities = new TaskPostingEntities();




        internal int SaveTask(tbl_Task stbl_Task)
        {
            sTaskPostingEntities.tbl_Task.Add(stbl_Task);
            sTaskPostingEntities.SaveChanges();
            return 1;
        }

        internal List<tbl_Task> SaveGetAllTaskTask()
        {
            try
            {
                var query = (from dept in sTaskPostingEntities.tbl_Task
                             select dept).OrderBy(x => x.Id);


                return query.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        internal List<tbl_Task> GetTaskById(string taskId)
        {
            long id = Convert.ToInt64(taskId);
            try
            {
                var query = (from dept in sTaskPostingEntities.tbl_Task
                             select dept).OrderBy(x => x.Id);


                return query.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }

        }

        internal int UpdateTask(tbl_Task stbl_Task, long _Id)
        {
            tbl_Task obj = sTaskPostingEntities.tbl_Task.First(x => x.Id == _Id);
            obj.Task = stbl_Task.Task;
            obj.Remarks = stbl_Task.Remarks;
            sTaskPostingEntities.SaveChanges();
            return 1;
        }

        internal int SaveTask(tbl_TaskPosting stbl_TaskPosting)
        {
            sTaskPostingEntities.tbl_TaskPosting.Add(stbl_TaskPosting);
            sTaskPostingEntities.SaveChanges();
            return 1;
        }

        internal List<taskR> GetAllPstingData()
        {
            try
            {
                return (from b in sTaskPostingEntities.tbl_TaskPosting
                        join p in sTaskPostingEntities.tbl_Task on b.Task_Id equals p.Id
                        select new taskR
                        {
                            Id = b.Id,
                            Date = b.Date,
                            Task = p.Task,
                            UserName = b.UserName,
                            Comments=b.Comments,
                            Tasl_Like=b.Tasl_Like,
                            Task_Dislike=b.Task_Dislike
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        internal List<tbl_TaskPosting> GetTotallikeCount(int id)
        {
           
            try
            {
                var query = (from dept in sTaskPostingEntities.tbl_TaskPosting
                             where dept.Id == id
                             select dept).OrderBy(x => x.Id);


                return query.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        internal int LikeUpdate(int id, int likeIncrese)
        {
            tbl_TaskPosting obj = sTaskPostingEntities.tbl_TaskPosting.First(x => x.Id == id);
            obj.Tasl_Like = likeIncrese;
            sTaskPostingEntities.SaveChanges();
            return 1;
        }

        internal int DisLikeUpdate(int id, int DislikeIncrese)
        {
            tbl_TaskPosting obj = sTaskPostingEntities.tbl_TaskPosting.First(x => x.Id == id);
            obj.Task_Dislike = DislikeIncrese;
            sTaskPostingEntities.SaveChanges();
            return 1;
        }
    }
}
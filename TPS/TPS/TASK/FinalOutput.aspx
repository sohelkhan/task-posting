﻿<%@ Page Title="Task Posting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FinalOutput.aspx.cs" Inherits="TPS.TASK.FinalOutput" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../css/responsive.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-wrap mg-t-40">
                    <div class="cmp-tb-hd cmp-int-hd">
                        <h2>Final Output</h2>
                        <asp:HiddenField ID="hidTask" runat="server" />
                        <asp:Label ID="lblMessageLabel" runat="server"></asp:Label>
                    </div>
                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"  style="margin-bottom:5px">
                        <div class="nk-int-st">
                                            <asp:DropDownList ID="ddlTask" class="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTask_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                  </div>
                   
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="margin-bottom:5px">
                        <asp:GridView ID="gridOutput" runat="server" AutoGenerateColumns="False" Width="100%"
                            CellPadding="5" AllowPaging="True" PageSize="10" CssClass="table table-bordered table-hover" OnPageIndexChanging="gridOutput_PageIndexChanging">
                            <Columns>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="MistyRose">
                                    <HeaderTemplate>
                                        Sl.
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSRNO" runat="server"
                                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Task" HeaderText="Task" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>

                                <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UserName" HeaderText="Admin" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>

                                <asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>
                                  <asp:BoundField DataField="Tasl_Like" HeaderText="Like" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>
                                  <asp:BoundField DataField="Task_Dislike" HeaderText="Dislike" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>
                               


                             
                            </Columns>
                            <EmptyDataRowStyle ForeColor="Red" />
                            <RowStyle CssClass="Grid_RowStyle" />
                            <AlternatingRowStyle CssClass="Grid_AltRowStyle" />
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" CssClass="pagination01 pageback" />
                            <HeaderStyle Width="10%" VerticalAlign="Middle" CssClass="Grid_Header" />
                            <FooterStyle CssClass="Grid_Footer" />
                        </asp:GridView>
                    </div>


                </div>
            </div>
        </div> 
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS.TASK.BLL;
using TPS.TASK.DAL;
using TPS.TASK.Repository;

namespace TPS.TASK
{
    public partial class FinalOutput : System.Web.UI.Page
    {
        taskBLL staskBLL = new taskBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllPstingData();
                GetAllTask();
            }
        }
        private void GetAllTask()
        {
            try
            {

                List<tbl_Task> objtblTaskPosing = new List<tbl_Task>();

                objtblTaskPosing = staskBLL.GetAllTask().ToList();
                if (objtblTaskPosing.Count > 0)
                {
                    ddlTask.DataSource = objtblTaskPosing.ToList();
                    ddlTask.DataTextField = "Task";
                    ddlTask.DataValueField = "Id";
                    ddlTask.DataBind();
                    ddlTask.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetAllPstingData()
        {
            List<taskR> objtblTaskPosting = new List<taskR>();

            objtblTaskPosting = staskBLL.GetAllPstingData().ToList();
            if (objtblTaskPosting.Count > 0)
            {
                gridOutput.DataSource = objtblTaskPosting;
                gridOutput.DataBind();
            }
        }

     

        protected void gridOutput_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridOutput.PageIndex = e.NewPageIndex;
            GetAllPstingData();
        }

        protected void ddlTask_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlTask.SelectedItem.Text != "--Select--")
            {
                List<taskR> objtblTaskPosting = new List<taskR>();
                string _task = ddlTask.SelectedItem.Text;
                objtblTaskPosting = staskBLL.GetAllPstingData().Where(x => x.Task == _task).ToList();
                if (objtblTaskPosting.Count > 0)
                {
                    gridOutput.DataSource = objtblTaskPosting;
                    gridOutput.DataBind();
                }
            }
            else
            {
                List<taskR> objtblTaskPosting = new List<taskR>();
                objtblTaskPosting = staskBLL.GetAllPstingData().ToList();
                if (objtblTaskPosting.Count > 0)
                {
                    gridOutput.DataSource = objtblTaskPosting;
                    gridOutput.DataBind();
                }
            }

           
        }
    }
}
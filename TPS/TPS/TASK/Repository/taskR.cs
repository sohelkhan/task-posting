﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPS.TASK.Repository
{
    public class taskR
    {
        public long Id { get; set; }
        public string Comments { get; set; }
        public string Task { get; set; }
        public DateTime? Date { get; set; }
        public string UserName { get; set; }
        public int? Tasl_Like { get; set; }
        public int? Task_Dislike { get; set; }
    }
}
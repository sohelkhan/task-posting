﻿<%@ Page Title="Task Posting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TaskLikeDislike.aspx.cs" Inherits="TPS.TASK.TaskLikeDislike" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../css/responsive.css" rel="stylesheet" />
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-example-wrap mg-t-40">
                    <div class="cmp-tb-hd cmp-int-hd">
                        <h2>Task Like And Dislike</h2>
                        <asp:HiddenField ID="hidTask" runat="server" />
                        <asp:Label ID="lblMessageLabel" runat="server"></asp:Label>
                    </div>
              
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <asp:GridView ID="gridTaskLikeDislike" runat="server" AutoGenerateColumns="False" Width="100%"
                            CellPadding="5" AllowPaging="True" PageSize="10" CssClass="table table-bordered table-hover" OnPageIndexChanging="gridTaskPosting_PageIndexChanging">
                            <Columns>

                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BackColor="MistyRose">
                                    <HeaderTemplate>
                                        Sl.
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSRNO" runat="server"
                                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Task" HeaderText="Task" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>

                                <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UserName" HeaderText="Admin" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="12%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>

                                <asp:BoundField DataField="Date" HeaderText="Date" HeaderStyle-BackColor="MistyRose">
                                    <HeaderStyle VerticalAlign="Middle" CssClass="Grid_Header" />
                                    <ItemStyle HorizontalAlign="Left" Width="15%" CssClass="Grid_Border" />
                                    <FooterStyle CssClass="Grid_Footer" />
                                </asp:BoundField>



                                <asp:TemplateField HeaderText="Like" HeaderStyle-BackColor="MistyRose" >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnLike" runat="server" ImageUrl="~/TASK/Img/like123.PNG" OnClick="imgbtnLike_Click" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="Grid_Border" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="DisLike" HeaderStyle-BackColor="MistyRose" >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgbtnClientDisLike" runat="server" ImageUrl="~/TASK/Img/Dislike123.PNG" OnClick="imgbtnClientDisLike_Click" />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="Grid_Border" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="Red" />
                            <RowStyle CssClass="Grid_RowStyle" />
                            <AlternatingRowStyle CssClass="Grid_AltRowStyle" />
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" CssClass="pagination01 pageback" />
                            <HeaderStyle Width="10%" VerticalAlign="Middle" CssClass="Grid_Header" />
                            <FooterStyle CssClass="Grid_Footer" />
                        </asp:GridView>
                    </div>


                </div>
            </div>
        </div> 
</asp:Content>


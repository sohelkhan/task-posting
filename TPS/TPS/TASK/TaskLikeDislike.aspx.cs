﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS.TASK.BLL;
using TPS.TASK.DAL;
using TPS.TASK.Repository;

namespace TPS.TASK
{
    public partial class TaskLikeDislike : System.Web.UI.Page
    {
        taskBLL staskBLL = new taskBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllPstingData();
            }
        }

        private void GetAllPstingData()
        {
            List<taskR> objtblTaskPosting = new List<taskR>();

            objtblTaskPosting = staskBLL.GetAllPstingData().ToList();
            if (objtblTaskPosting.Count > 0)
            {
                gridTaskLikeDislike.DataSource = objtblTaskPosting;
                gridTaskLikeDislike.DataBind();
            }
        }

       

     

    

        protected void imgbtnClientEdit_Click(object sender, ImageClickEventArgs e)
        {
            List<tbl_Task> obj = new List<tbl_Task>();
            ImageButton imgbtn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imgbtn.NamingContainer;

            try
            {
                string taskId = "";
                Label lblId = (Label)gridTaskLikeDislike.Rows[row.RowIndex].FindControl("lblId");
                if (lblId != null)
                {

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

    

        protected void gridTaskPosting_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridTaskLikeDislike.PageIndex = e.NewPageIndex;
            GetAllPstingData();
        }

        protected void imgbtnLike_Click(object sender, ImageClickEventArgs e)
        {            
            ImageButton imgbtn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imgbtn.NamingContainer;

            try
            {
                int like = 0;
                Label lblId = (Label)gridTaskLikeDislike.Rows[row.RowIndex].FindControl("lblId");
                int id = Convert.ToInt32(lblId.Text);
                List<tbl_TaskPosting> likeCount = staskBLL.GetTotallikeCount(id);


                var result = likeCount.First();

                 like =(int) result.Tasl_Like;

                 int likeIncrese = like + 1;

                 int likeupdate = staskBLL.LikeUpdate(id, likeIncrese);

              

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void imgbtnClientDisLike_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton imgbtn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imgbtn.NamingContainer;

            try
            {
                int like = 0;
                Label lblId = (Label)gridTaskLikeDislike.Rows[row.RowIndex].FindControl("lblId");
                int id = Convert.ToInt32(lblId.Text);
                List<tbl_TaskPosting> likeCount = staskBLL.GetTotallikeCount(id);


                var result = likeCount.First();

                like = (int)result.Task_Dislike;

                int DislikeIncrese = like + 1;

                int likeupdate = staskBLL.DisLikeUpdate(id, DislikeIncrese);



            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
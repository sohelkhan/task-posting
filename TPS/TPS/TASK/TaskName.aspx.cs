﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS.TASK.BLL;
using TPS.TASK.DAL;

namespace TPS.TASK
{
    public partial class TaskName : System.Web.UI.Page
    {
        taskBLL staskBLL = new taskBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllTask();
            }
        }

        private void GetAllTask()
        {
            try
            {

                List<tbl_Task> objtblTaskPosing = new List<tbl_Task>();

                objtblTaskPosing = staskBLL.GetAllTask().ToList();
                if (objtblTaskPosing.Count > 0)
                {
                    gridTaskName.DataSource = objtblTaskPosing;
                    gridTaskName.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            tbl_Task stbl_Task = new tbl_Task();

            stbl_Task.Task = txtTask.Text;
            stbl_Task.Remarks = txtRemarks.Text;
            stbl_Task.TaskPostingDate = DateTime.Now.Date;
            if (btnSave.Text == "Save")
            {
                int result = staskBLL.SaveTask(stbl_Task);
                lblMessageLabel.Text = "Data Save Successfully!";
            }
            else
            {
                long _Id = Convert.ToInt64(hidTask.Value);
                int result = staskBLL.UpdateTask(stbl_Task, _Id);
                if (result > 0)
                {

                    lblMessageLabel.Text = "Data Update successfully";
                }
            }
            GetAllTask();
            clearUI();
        }

        private void clearUI()
        {
            txtTask.Text = "";
            txtRemarks.Text = "";
            btnSave.Text = "Save";
        }

        protected void imgbtnClientEdit_Click(object sender, ImageClickEventArgs e)
        {
            List<tbl_Task> obj = new List<tbl_Task>();
            ImageButton imgbtn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imgbtn.NamingContainer;

            try
            {
                string taskId = "";
                Label lblId = (Label)gridTaskName.Rows[row.RowIndex].FindControl("lblId");
                if (lblId != null)
                {

                    taskId = lblId.Text;
                    obj = staskBLL.GetTaskById(taskId);

                    if (obj != null)
                    {
                        var result = obj.First();
                        hidTask.Value = result.Id.ToString();

                        txtTask.Text = result.Task;
                        txtRemarks.Text = result.Remarks;

                        if (btnSave.Text == "Save")
                        {
                            btnSave.Text = "Update";
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected void gridTaskName_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridTaskName.PageIndex = e.NewPageIndex;
            GetAllTask();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TPS.TASK.BLL;
using TPS.TASK.DAL;
using TPS.TASK.Repository;

namespace TPS.TASK
{
    public partial class TaskPosting : System.Web.UI.Page
    {
        taskBLL staskBLL = new taskBLL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetAllTask();
                GetAllPstingData();
            }
        }

        private void GetAllPstingData()
        {
            List<taskR> objtblTaskPosting = new List<taskR>();

            objtblTaskPosting = staskBLL.GetAllPstingData().ToList();
            if (objtblTaskPosting.Count > 0)
            {
                gridTaskPosting.DataSource = objtblTaskPosting;
                gridTaskPosting.DataBind();
            }
        }

        private void GetAllTask()
        {
            try
            {

                List<tbl_Task> objtblTaskPosing = new List<tbl_Task>();

                objtblTaskPosing = staskBLL.GetAllTask().ToList();
                if (objtblTaskPosing.Count > 0)
                {
                    ddlTask.DataSource = objtblTaskPosing.ToList();
                    ddlTask.DataTextField = "Task";
                    ddlTask.DataValueField = "Id";
                    ddlTask.DataBind();
                    ddlTask.Items.Insert(0, new ListItem("--Select--", "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            tbl_TaskPosting stbl_TaskPosting = new tbl_TaskPosting();

            stbl_TaskPosting.Task_Id = Convert.ToInt32(ddlTask.SelectedValue);
            stbl_TaskPosting.Comments = txtComments.Text;
            stbl_TaskPosting.UserName = "User1";
            stbl_TaskPosting.Date = DateTime.Now.Date;
            if (btnSave.Text == "Save")
            {
                int result = staskBLL.SaveTask(stbl_TaskPosting);
                lblMessageLabel.Text = "Data Save Successfully!";
            }
            else
            {
                //long _Id = Convert.ToInt64(hidTask.Value);
                //int result = staskBLL.UpdateTask(stbl_TaskPosting, _Id);
                //if (result > 0)
                //{

                //    lblMessageLabel.Text = "Data Update successfully";
                //}
            }
            GetAllTask();
            clearUI();
        }

        private void clearUI()
        {
            ddlTask.ClearSelection();
            txtComments.Text = "";
            btnSave.Text = "Save";
        }

        protected void imgbtnClientEdit_Click(object sender, ImageClickEventArgs e)
        {
            List<tbl_Task> obj = new List<tbl_Task>();
            ImageButton imgbtn = (ImageButton)sender;
            GridViewRow row = (GridViewRow)imgbtn.NamingContainer;

            try
            {
                string taskId = "";
                Label lblId = (Label)gridTaskPosting.Rows[row.RowIndex].FindControl("lblId");
                if (lblId != null)
                {

                    taskId = lblId.Text;
                    obj = staskBLL.GetTaskById(taskId);

                    if (obj != null)
                    {
                        var result = obj.First();
                        hidTask.Value = result.Id.ToString();

                       // txtTask.Text = result.Task;
                        txtComments.Text = result.Remarks;

                        if (btnSave.Text == "Save")
                        {
                            btnSave.Text = "Update";
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
            

        protected void gridTaskPosting_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridTaskPosting.PageIndex = e.NewPageIndex;
            GetAllPstingData();
        }
    }
}